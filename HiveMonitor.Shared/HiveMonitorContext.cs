﻿using HiveMonitor.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace HiveMonitor.Shared
{
    public partial class HiveMonitorContext : DbContext
    {
        public HiveMonitorContext()
        {
        }

        public HiveMonitorContext(DbContextOptions<HiveMonitorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Sample> Samples { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseNpgsql(@"Server=127.0.0.1;Port=5432;Database=HiveMonitor;Integrated Security=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
