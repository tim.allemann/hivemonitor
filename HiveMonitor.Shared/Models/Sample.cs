﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HiveMonitor.Shared.Models
{
    [Table("Samples", Schema = "Hive")]
    public partial class Sample
    {
        public Sample()
        {
            
        }

        [Key]
        public int SampleId { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        public float? Weight { get; set; }
        public float? TemperatureInside { get; set; }
        public float? TemperatureOutside { get; set; }


    }
}
